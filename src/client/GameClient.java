package client;

import client.lib.HistoryMap;
import util.Campo;
import util.PDU;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;

public class GameClient {
    private static final boolean WITH_SECURITY = false;
    private final static int PACKETSIZE = 1400;

    private DatagramSocket socket;
    private InetAddress ip;
    private int port;
    private short packetCount = 0;
    private HistoryMap history;

    public GameClient(InetAddress ip, int port) throws UnknownHostException, SocketException {
        this.ip = ip;
        this.port = port;

        this.socket = new DatagramSocket();
        this.socket.setSoTimeout(3000);

        this.history = new HistoryMap(20);
    }

    public InetAddress getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public ArrayList<PDU> receive() throws IOException {
        DatagramPacket incomingPacket = new DatagramPacket(new byte[PACKETSIZE], PACKETSIZE);

        ArrayList<PDU> ret = new ArrayList();

        PDU incomingPDU;

        System.out.println("starting to receive.. w8ing..");

        do {
            this.socket.receive(incomingPacket);

            System.out.println("received one");

            incomingPDU = new PDU(incomingPacket.getData());

            ret.add(incomingPDU);
        } while (incomingPDU.getCampos().containsKey(Campo.COD_LISTA_CONTINUA));

        System.out.println("received...");

        return ret;
    }

    private ArrayList<PDU> dispatchPDU(PDU pdu, boolean waitForAnswer) throws IOException {
        pdu.setLabel(++packetCount);

        DatagramPacket packet = new DatagramPacket(pdu.toBytes(), pdu.length(), this.ip, this.port);

        socket.send(packet);

        if (waitForAnswer) {
            DatagramPacket incomingPacket = new DatagramPacket(new byte[PACKETSIZE], PACKETSIZE);

            ArrayList<PDU> ret = new ArrayList<PDU>();

            PDU incomingPDU;

            do {
                this.socket.receive(incomingPacket);

                incomingPDU = new PDU(incomingPacket.getData());

                System.out.println(incomingPDU);

                ret.add(incomingPDU);
            } while (incomingPDU.getCampos().containsKey(Campo.COD_LISTA_CONTINUA));

            return ret;
        } else return null;
    }

    public PDU hello() throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_HELLO);

        DatagramPacket packet = new DatagramPacket(pdu.toBytes(), pdu.length(), this.ip, this.port);

        socket.send(packet);

        DatagramPacket incomingPacket = new DatagramPacket(new byte[PACKETSIZE], PACKETSIZE);

        this.socket.receive(incomingPacket);

        util.PDU receivedPDU = new util.PDU(incomingPacket.getData());

        return receivedPDU;
    }

    public PDU register(String nome, String alcunha, String sec_info) throws IOException {
        this.socket.setSoTimeout(0);

        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_REGISTER);

        pdu.getCampos().put(Campo.COD_NOME_UTILIZADOR, new Campo(Campo.COD_NOME_UTILIZADOR, nome));
        pdu.getCampos().put(Campo.COD_ALCUNHA, new Campo(Campo.COD_ALCUNHA, alcunha));
        pdu.getCampos().put(Campo.COD_SEC_INFO, new Campo(Campo.COD_SEC_INFO, sec_info));

        return dispatchPDU(pdu, true).get(0);
    }

    public PDU login(String alcunha, String sec_info) throws IOException {
        this.socket.setSoTimeout(0);

        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_LOGIN);

        pdu.getCampos().put(Campo.COD_ALCUNHA, new Campo(Campo.COD_ALCUNHA, alcunha));
        pdu.getCampos().put(Campo.COD_SEC_INFO, new Campo(Campo.COD_SEC_INFO, sec_info));

        return dispatchPDU(pdu, true).get(0);
    }

    public PDU logout() throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_LOGOUT);
        return dispatchPDU(pdu, true).get(0);
    }

    public PDU quit() throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_QUIT);
        return dispatchPDU(pdu, true).get(0);
    }

    public PDU end() throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_END);
        return dispatchPDU(pdu, true).get(0);
    }

    public ArrayList<PDU> list_challenges() throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_LIST_CHALLENGES);
        return dispatchPDU(pdu, true);
    }

    public PDU make_challenge(String nome_desafio, String data, String hora) throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_MAKE_CHALLENGE);

        pdu.getCampos().put(Campo.COD_NOME_DESAFIO, new Campo(Campo.COD_NOME_DESAFIO, nome_desafio));

        if (!data.isEmpty() && !hora.isEmpty()) {
            pdu.getCampos().put(Campo.COD_DATA, new Campo(Campo.COD_DATA, data));
            pdu.getCampos().put(Campo.COD_HORA, new Campo(Campo.COD_HORA, hora));
        }

        return dispatchPDU(pdu, true).get(0);
    }

    public PDU accept_challenge(String nome_desafio) throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_ACCEPT_CHALLENGE);

        pdu.getCampos().put(Campo.COD_NOME_DESAFIO, new Campo(Campo.COD_NOME_DESAFIO, nome_desafio));

        return dispatchPDU(pdu, true).get(0);
    }

    public void delete_challenge(String nome_desafio) throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_DELETE_CHALLENGE);

        pdu.getCampos().put(Campo.COD_NOME_DESAFIO, new Campo(Campo.COD_NOME_DESAFIO, nome_desafio));

        dispatchPDU(pdu, false);
    }

    public PDU answer(byte escolha, String nome_desafio, byte nr_questao) throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_ANSWER);

        pdu.getCampos().put(Campo.COD_ESCOLHA, new Campo(Campo.COD_ESCOLHA, escolha));
        pdu.getCampos().put(Campo.COD_NOME_DESAFIO, new Campo(Campo.COD_NOME_DESAFIO, nome_desafio));
        pdu.getCampos().put(Campo.COD_NR_QUESTAO, new Campo(Campo.COD_NR_QUESTAO, nr_questao));

        return dispatchPDU(pdu, true).get(0);
    }

    public PDU retransmit(String nome_desafio, byte nr_questao, byte nr_bloco_audio) throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_RETRANSMIT);

        pdu.getCampos().put(Campo.COD_NOME_DESAFIO, new Campo(Campo.COD_NOME_DESAFIO, nome_desafio));
        pdu.getCampos().put(Campo.COD_NR_QUESTAO, new Campo(Campo.COD_NR_QUESTAO, nr_questao));
        pdu.getCampos().put(Campo.COD_NR_BLOCO_AUDIO, new Campo(Campo.COD_NR_BLOCO_AUDIO, nr_bloco_audio));

        return dispatchPDU(pdu, true).get(0);
    }

    public ArrayList<PDU> list_ranking() throws IOException {
        PDU pdu = new PDU(WITH_SECURITY, PDU.TIPO_LIST_RANKING);

        return dispatchPDU(pdu, true);
    }
}
