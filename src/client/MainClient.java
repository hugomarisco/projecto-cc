package client;

import client.controllers.WelcomeController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.InetAddress;

public class MainClient extends Application {
    public static void main(String [] args) throws IOException {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        GameClient client;

        try {
            System.out.println("Tentando ligar ao servidor 255.255.255.255:4444");

            client = new GameClient(InetAddress.getByName("255.255.255.255"), 4444);

            client.hello();
        } catch (IOException e1) {
            try {
                System.out.println("Tentando ligar ao servidor 127.0.0.1:4444");

                client = new GameClient(InetAddress.getByName("127.0.0.1"), 4444);

                client.hello();
            } catch (IOException e2) {
                System.out.println("Não foi possível encontrar o servidor na rede.");
                Platform.exit();
                return;
            }
        }

        primaryStage.setTitle("SongJazz");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("views/welcome.fxml"));

        Pane pane = loader.load();

        WelcomeController controller = loader.getController();

        controller.initData(client);

        Scene myScene = new Scene(pane);

        primaryStage.setScene(myScene);
        primaryStage.show();
    }
}
