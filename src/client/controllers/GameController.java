package client.controllers;

import client.GameClient;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import util.Jogo;
import util.Pergunta;
import util.UtilizadorLocal;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.ResourceBundle;

public class GameController implements Initializable {

    private GameClient cliente;
    private Pergunta pergunta;
    @FXML
    public Label lblPergunta, lblNrPergunta;
    public Button btnOpcao1, btnOpcao2, btnOpcao3;
    public ImageView img;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void initData(GameClient cliente, Pergunta pergunta) {
        this.cliente = cliente;
        this.pergunta = pergunta;

        lblPergunta.textProperty().bind(this.pergunta.perguntaProperty());

        System.out.println(btnOpcao1.getText());

        System.out.println(this.pergunta.opcao1Property().get());

        btnOpcao1.textProperty().bind(this.pergunta.opcao1Property());
        btnOpcao2.textProperty().bind(this.pergunta.opcao2Property());
        btnOpcao3.textProperty().bind(this.pergunta.opcao3Property());

        System.out.println(this.pergunta.getImg().getHeight());

        img.setImage(this.pergunta.getImg());
    }
}
