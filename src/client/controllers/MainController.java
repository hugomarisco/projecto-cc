package client.controllers;

import client.GameClient;
import client.MainClient;
import client.WaitForGame;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import util.*;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    private GameClient cliente;
    private UtilizadorLocal utilizador;

    @FXML protected Label nomeLbl, alcunhaLbl, scoreLbl;
    @FXML protected TextField gameNameTxt, gameHourPicker, gameDatePicker;
    @FXML protected ObservableList<Jogo> jogos;
    @FXML protected ObservableList<Utilizador> utilizadores;
    @FXML protected TableView jogosTbl, utilizadoresTbl;
    @FXML protected TabPane mainTab;
    @FXML protected Button btnCancelGame, btnUpdateGames, btnUpdateRankings;
    @FXML protected VBox vboxCreateGame;

    public void initData(GameClient cliente, UtilizadorLocal utilizador) {
        this.cliente = cliente;
        this.utilizador = utilizador;

        btnCancelGame.visibleProperty().bind(utilizador.bloqueadoProperty());

        vboxCreateGame.disableProperty().bind(utilizador.bloqueadoProperty());

        btnUpdateGames.disableProperty().bind(utilizador.bloqueadoProperty());
        btnUpdateRankings.disableProperty().bind(utilizador.bloqueadoProperty());

        nomeLbl.textProperty().bind(this.utilizador.alcunhaProperty());
        alcunhaLbl.textProperty().bind(this.utilizador.alcunhaProperty());
        scoreLbl.textProperty().bind(this.utilizador.scoreProperty().asString());

        this.jogos = FXCollections.observableArrayList();
        this.utilizadores = FXCollections.observableArrayList();

        TableColumn colJogar = new TableColumn<>("Jogar");

        colJogar.visibleProperty().bind(this.utilizador.bloqueadoProperty().not());

        jogosTbl.getColumns().add(colJogar);

        colJogar.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<Jogo, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Jogo, Boolean> p) {
                    return new SimpleBooleanProperty(p.getValue() != null);
                }
            }
        );

        final GameClient _cliente = this.cliente;
        final UtilizadorLocal _utilizador = this.utilizador;

        colJogar.setCellFactory(
            new Callback<TableColumn<Jogo, Boolean>, TableCell<Jogo, Boolean>>() {
                @Override
                public TableCell<Jogo, Boolean> call(TableColumn<Jogo, Boolean> p) {
                    return new ButtonCell(_cliente, _utilizador);
                }
            }
        );

        this.jogosTbl.setItems(this.jogos);
        this.utilizadoresTbl.setItems(this.utilizadores);
    }

    @FXML
    protected void createGame(ActionEvent event) throws IOException, ParseException {
        String gameName = gameNameTxt.getText();
        String gameHour = gameHourPicker.getText().replace(":", "");
        String gameDate = gameDatePicker.getText().replace("/", "");

        PDU pdu = cliente.make_challenge(gameName, gameDate, gameHour);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        if (!pdu.hasError()) {

            utilizador.setBloqueado(true);

            alert.setHeaderText("O jogo foi criado com sucesso!");
            alert.setTitle("Sucesso!");
            alert.setContentText("Aguarde até que o jogo comece!");

            alert.show();

            new Thread(new WaitForGame(this.cliente, this.utilizador)).start();
        } else {
            Alert errorAlert = new Alert(Alert.AlertType.ERROR);

            errorAlert.setHeaderText("Ocorreu um erro!");
            errorAlert.setTitle("Ocorreu um erro!");
            errorAlert.setContentText(pdu.getError());

            errorAlert.show();
        }
    }

    @FXML
    protected void getGames(ActionEvent event) throws ParseException, IOException {
        ArrayList<PDU> jogos = this.cliente.list_challenges();

        if (jogos.get(0).hasError()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);

            alert.setHeaderText("Aviso!");
            alert.setTitle("Aviso!");
            alert.setContentText(jogos.get(0).getError());

            alert.show();
        } else {
            this.jogos.clear();

            for (PDU pdu : jogos) {
                String nome = new String(pdu.getCampos().get(Campo.COD_NOME_DESAFIO).getValue().array());
                String data = new String(pdu.getCampos().get(Campo.COD_DATA).getValue().array());
                String hora = new String(pdu.getCampos().get(Campo.COD_HORA).getValue().array());

                Jogo j = new Jogo(nome, data, hora);

                this.jogos.add(j);
            }
        }
    }

    @FXML
    protected void cancelGame(ActionEvent event) throws IOException {
        this.cliente.delete_challenge(gameNameTxt.getText());
    }

    @FXML
    protected void getUsers(ActionEvent event) {
        try {
            ArrayList<PDU> utilizadores = this.cliente.list_ranking();

            this.utilizadores.clear();

            for (PDU pdu : utilizadores) {
                String alcunha = new String(pdu.getCampos().get(Campo.COD_ALCUNHA).getValue().array());
                short score = pdu.getCampos().get(Campo.COD_SCORE).getValue().getShort();

                Utilizador u = new Utilizador(alcunha, score);

                this.utilizadores.add(u);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    private class ButtonCell extends TableCell<Jogo, Boolean> {
        final Button cellButton = new Button("Jogar");

        ButtonCell(final GameClient cliente, final UtilizadorLocal utilizador){

            cellButton.setOnAction(new EventHandler<ActionEvent>(){

                @Override
                public void handle(ActionEvent t) {
                Jogo currentGame = ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());

                try {
                    cliente.accept_challenge(currentGame.getNome());

                    utilizador.setBloqueado(true);

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);

                    alert.setHeaderText("Aderiu ao jogo com sucesso!");
                    alert.setTitle("Sucesso!");
                    alert.setContentText("Aguarde até que o jogo comece!");

                    alert.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                }
            });
        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(cellButton);
            } else {
                setGraphic(null);
            }
        }
    }
}
