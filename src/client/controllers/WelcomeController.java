package client.controllers;

import client.GameClient;
import client.MainClient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import util.Campo;
import util.PDU;
import util.UtilizadorLocal;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class WelcomeController implements Initializable {
    @FXML private TextField loginUsernameField, loginPasswordField, registerNameField, registerUsernameField, registerPasswordField;
    @FXML private Label loginStatusLabel;

    private GameClient client;

    public void initData(GameClient client) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        this.client = client;

        loginStatusLabel.setText("Conectado ao servidor " + client.getIp() + ":" + client.getPort());

        /*File audioFile = new File("assets/musica/000003.wav");

        Media hit = new Media("file://" + audioFile.getAbsolutePath().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.play();*/
    }

    @FXML
    protected void entrar(ActionEvent event) {
        String alcunha = loginUsernameField.getText();
        String sec_info = loginPasswordField.getText();

        Button btnLogin = (Button) event.getSource();

        btnLogin.setText("Entrando...");

        Alert alert = new Alert(Alert.AlertType.ERROR);

        try {
            PDU pdu = this.client.login(alcunha, sec_info);

            if (pdu.hasError()) {
                alert.setHeaderText("Erro ao tentar entrar!");
                alert.setTitle("Erro!");
                alert.setContentText(pdu.getError());

                alert.show();

                btnLogin.setText("Entrar");
            } else {
                String nome = new String(pdu.getCampos().get(Campo.COD_NOME_UTILIZADOR).getValue().array());
                short score = pdu.getCampos().get(Campo.COD_SCORE).getValue().getShort();

                launchMain((Stage) btnLogin.getScene().getWindow(), new UtilizadorLocal(nome, alcunha, sec_info, score));
            }
        } catch (IOException e) {
            btnLogin.setText("Entrar");
            e.printStackTrace();
        }
    }

    @FXML
    protected void registar(ActionEvent event) {
        String alcunha = registerUsernameField.getText();
        String nome = registerNameField.getText();
        String sec_info = registerPasswordField.getText();

        Button btnRegister = (Button) event.getSource();

        btnRegister.setText("Registando...");

        Alert alert = new Alert(Alert.AlertType.ERROR);

        try {
            PDU pdu = this.client.register(nome, alcunha, sec_info);

            if (pdu.hasError()) {
                alert.setHeaderText("Erro ao tentar registar!");
                alert.setTitle("Erro!");
                alert.setContentText(pdu.getError());

                alert.show();

                btnRegister.setText("Registar");
            } else {
                short score = pdu.getCampos().get(Campo.COD_SCORE).getValue().getShort();

                Stage window = (Stage) btnRegister.getScene().getWindow();

                launchMain(window, new UtilizadorLocal(nome, alcunha, sec_info));
            }
        } catch (IOException e) {
            btnRegister.setText("Registar");
            e.printStackTrace();
        }
    }

    private void launchMain(Stage window, UtilizadorLocal utilizador) throws IOException {
        FXMLLoader loader = new FXMLLoader(MainClient.class.getResource("views/main.fxml"));

        Parent root = loader.load();

        MainController controller = loader.getController();

        controller.initData(this.client, utilizador);

        Scene scene = new Scene(root);

        window.setScene(scene);

        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
