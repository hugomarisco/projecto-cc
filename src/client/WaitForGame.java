package client;

import client.controllers.GameController;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import util.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public class WaitForGame implements Runnable {
    private GameClient client;
    private UtilizadorLocal utilizador;
    private Jogo jogo;

    public WaitForGame(GameClient client, UtilizadorLocal utilizador) {
        this.client = client;
        this.utilizador = utilizador;
    }

    @Override
    public void run() {
        try {
            final ArrayList<PDU> res = client.receive();

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    if (!res.get(0).hasError()) {
                        ByteBuffer imgbuf = ByteBuffer.allocate(10000000);
                        Pergunta p = null;

                        for (PDU pdu : res) {
                            if (pdu.getCampos().containsKey(Campo.COD_QUESTAO)) {
                                String pergunta = new String(pdu.getCampos().get(Campo.COD_QUESTAO).getValue().array());
                                String opcao1 = new String(pdu.getCampos().get(Campo.COD_RESPOSTA1).getValue().array());
                                String opcao2 = new String(pdu.getCampos().get(Campo.COD_RESPOSTA2).getValue().array());
                                String opcao3 = new String(pdu.getCampos().get(Campo.COD_RESPOSTA3).getValue().array());

                                int certa = pdu.getCampos().get(Campo.COD_CERTA).getValue().getShort();

                                p = new Pergunta(pergunta, opcao1, opcao2, opcao3, certa);
                            }

                            if (pdu.getCampos().containsKey(Campo.COD_IMAGEM)) {
                                System.out.println("lala " + pdu.getCampos().get(Campo.COD_IMAGEM).getValue().array());
                                imgbuf.put(pdu.getCampos().get(Campo.COD_IMAGEM).getValue().array());
                            }
                        }

                        p.setImgBytes(imgbuf);

                        FXMLLoader loader = new FXMLLoader(MainClient.class.getResource("views/game.fxml"));

                        Parent root = null;
                        try {
                            root = loader.load();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        GameController controller = loader.getController();

                        controller.initData(client, p);

                        Stage stage = new Stage();

                        stage.setScene(new Scene(root));

                        stage.show();
                    } else {
                        Alert errorAlert = new Alert(Alert.AlertType.ERROR);

                        errorAlert.setHeaderText("Bah!");
                        errorAlert.setTitle("Bah!");
                        errorAlert.setContentText(res.get(0).getError());

                        errorAlert.show();

                        utilizador.setBloqueado(false);
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
