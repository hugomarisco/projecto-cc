package util;

import java.nio.ByteBuffer;
import java.util.HashMap;

public class ListaCampos extends HashMap<Byte, Campo> {

    public ListaCampos() {
        super();
    }

    public ListaCampos(byte[] raw) {
        super();

        ByteBuffer rawBuffer = ByteBuffer.wrap(raw);

        while (rawBuffer.remaining() > 0) {
            byte cod = rawBuffer.get();
            byte length = rawBuffer.get();

            System.out.println("length..." + length);

            byte[] value = new byte[length];

            System.out.println(length);

            rawBuffer.get(value, 0, length);

            Campo c = new Campo(cod, new String(value));

            this.put(c.getCod(), c);
        }
    }

    public int length() {
        short sum = 0;

        for (Campo c : this.values()) {
            sum += c.size();
        }

        return sum;
    }

    public byte[] toBytes() {
        ByteBuffer ret = ByteBuffer.allocate(this.length());

        for (Campo c : this.values()) {
            ret.put(c.toBytes());
        }

        return ret.array();
    }

    public String toString() {
        String ret = "[";

        for (Campo c : this.values()) ret += c.toString() + ", ";

        ret += "]";

        return ret;
    }
}
