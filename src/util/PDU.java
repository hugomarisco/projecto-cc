package util;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class PDU {

    public final static int PACKETSIZE = 1400;

    public static final byte TIPO_REPLY = 0;
    public static final byte TIPO_HELLO = 1;
    public static final byte TIPO_REGISTER = 2;
    public static final byte TIPO_LOGIN = 3;
    public static final byte TIPO_LOGOUT = 4;
    public static final byte TIPO_QUIT = 5;
    public static final byte TIPO_END = 6;
    public static final byte TIPO_LIST_CHALLENGES = 7;
    public static final byte TIPO_MAKE_CHALLENGE = 8;
    public static final byte TIPO_ACCEPT_CHALLENGE = 9;
    public static final byte TIPO_DELETE_CHALLENGE = 10;
    public static final byte TIPO_ANSWER = 11;
    public static final byte TIPO_RETRANSMIT = 12;
    public static final byte TIPO_LIST_RANKING = 13;

    private byte version = 0;
    private byte security = 0;
    private short label = 0;
    private byte type;
    private ListaCampos campos;

    public PDU(byte[] raw) {
        ByteBuffer rawBuffer = ByteBuffer.wrap(raw);

        this.version = rawBuffer.get();
        this.security = rawBuffer.get();
        this.label = rawBuffer.getShort();
        this.type = rawBuffer.get();

        byte numberOfFields = rawBuffer.get();
        short sizeOfFields = rawBuffer.getShort();

        byte[] rawFields = new byte[sizeOfFields];

        rawBuffer.get(rawFields, 0, sizeOfFields);

        this.campos = new ListaCampos(rawFields);
    }

    public PDU(boolean secure, byte type) {
        if (secure) this.security = (byte) 1;

        this.type = type;

        this.campos = new ListaCampos();
    }

    public byte getVersion() {
        return this.version;
    }

    public byte getSecurity() {
        return this.security;
    }

    public void setLabel(short label) {
        this.label = label;
    }

    public short getLabel() {
        return this.label;
    }

    public byte getType() {
        return this.type;
    }

    public int getNumberOfFields() {
        return this.campos.size();
    }

    public int getSizeOfFields() {
        return this.campos.length();
    }

    public ListaCampos getCampos() {
        return this.campos;
    }

    public boolean hasError() {
        System.out.println(this.campos.keySet().toString());
        System.out.println(Campo.COD_ERRO);
        System.out.println(this.campos.containsKey(new Byte(Campo.COD_ERRO)));
        return this.campos.containsKey(new Byte(Campo.COD_ERRO));
    }

    public String getError() {
        return new String(this.campos.get(Campo.COD_ERRO).getValue().array());
    }

    public int length() {
        return 8 + this.campos.length();
    }

    public byte[] toBytes() {
        ByteBuffer ret = ByteBuffer.allocate(this.length());

        ret.put(this.version);
        ret.put(this.security);
        ret.putShort(this.label);
        ret.put(this.type);
        ret.put((byte) this.campos.size());
        ret.putShort((short) this.campos.length());

        ret.put(this.campos.toBytes());

        return ret.array();
    }

    @Override
    public String toString() {
        return "util.PDU{" +
                "version=" + version +
                ", security=" + security +
                ", label=" + label +
                ", type=" + type +
                ", nr_fields=" + getNumberOfFields() +
                ", size_fields=" + getSizeOfFields() +
                ", campos=" + campos +
                '}';
    }
}
