package util;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class Jogo {
    private SimpleStringProperty nome, data, hora;
    private SimpleObjectProperty<Date> data_hora;
    private ArrayList<UtilizadorLocal> participantes;
    private final String desafio_id = "000001";
    private HashSet<Pergunta> perguntas;
    private Iterator<Pergunta> perguntasIterator;
    private int nrPergunta;

    public Jogo(String nome, String data, String hora) throws ParseException, IOException {
        this.nome = new SimpleStringProperty(nome);
        this.data_hora = new SimpleObjectProperty(Campo.dateHourFormat().parse(data + " " + hora));
        this.data = new SimpleStringProperty(data);
        this.hora = new SimpleStringProperty(hora);
        this.participantes = new ArrayList<>();
        this.perguntas = new HashSet<>();
        this.perguntasIterator = this.perguntas.iterator();
        this.nrPergunta = 1;

        BufferedReader br = new BufferedReader(new FileReader("assets/desafio-" + desafio_id + ".txt"));

        String line = null;

        Pergunta p;

        int lineno = 1;

        while ((line = br.readLine()) != null) {
            if (lineno >= 4) {
                String[] parts = line.split(",");

                p = new Pergunta(parts[2], parts[3], parts[4], parts[5], Integer.parseInt(parts[6]));

                this.perguntas.add(p);

                this.perguntasIterator = this.perguntas.iterator();
            }

            lineno++;
        }

        br.close();
    }

    public int getNrPergunta() {
        return this.nrPergunta;
    }

    public boolean haMaisPerguntas() {
        return this.perguntasIterator.hasNext();
    }

    public String imagemActualPath() {
        return "assets/imagens/" + String.format("%6s", nrPergunta ).replace(' ', '0') + ".jpg";
    }

    public Pergunta proximaPergunta() {
        this.nrPergunta++;
        return this.perguntasIterator.next();
    }

    public boolean adicionarParticipante(UtilizadorLocal participante) {
        return this.participantes.add(participante);
    }

    public ArrayList<UtilizadorLocal> getParticipantes() {
        return participantes;
    }

    public String getNome() {
        return nome.get();
    }

    public SimpleStringProperty nomeProperty() {
        return nome;
    }

    public String getData() {
        return data.get();
    }

    public String getHora() {
        return hora.get();
    }

    public SimpleStringProperty dataProperty() {
        return data;
    }

    public SimpleStringProperty horaProperty() {
        return hora;
    }

    public SimpleObjectProperty<Date> data_horaProperty() {
        return this.data_hora;
    }

    public Date getDataHora() {
        return data_hora.get();
    }
}
