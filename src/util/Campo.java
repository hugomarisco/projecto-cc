package util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;

public class Campo {
    public static final byte COD_OK = (byte) 0;
    public static final byte COD_NOME_UTILIZADOR = (byte) 1;
    public static final byte COD_ALCUNHA = (byte) 2;
    public static final byte COD_SEC_INFO = (byte) 3;
    public static final byte COD_DATA = (byte) 4;
    public static final byte COD_HORA = (byte) 5;
    public static final byte COD_ESCOLHA = (byte) 6;
    public static final byte COD_NOME_DESAFIO = (byte) 7;
    public static final byte COD_NR_QUESTAO = (byte) 10;
    public static final byte COD_QUESTAO = (byte) 11;
    public static final byte COD_NR_RESPOSTA = (byte) 12;
    public static final byte COD_RESPOSTA1 = (byte) 30;
    public static final byte COD_RESPOSTA2 = (byte) 31;
    public static final byte COD_RESPOSTA3 = (byte) 32;
    public static final byte COD_CERTA = (byte) 14;
    public static final byte COD_PONTOS = (byte) 15;
    public static final byte COD_IMAGEM = (byte) 16;
    public static final byte COD_NR_BLOCO_AUDIO = (byte) 17;
    public static final byte COD_AUDIO = (byte) 18;
    public static final byte COD_SCORE = (byte) 20;
    public static final byte COD_MULTICAST_GROUP = (byte) 253;
    public static final byte COD_LISTA_CONTINUA = (byte) 254;
    public static final byte COD_ERRO = (byte) 255;

    public static final SimpleDateFormat dateFormat() {
        return new SimpleDateFormat("ddMMyy");
    }

    public static final SimpleDateFormat hourFormat() {
        return new SimpleDateFormat("HHmmss");
    }

    public static final SimpleDateFormat dateHourFormat() {
        return new SimpleDateFormat("ddMMyy HHmmss");
    }

    private byte cod;
    private byte[] value;

    public Campo(byte cod, String value) {
        this.cod = cod;
        this.value = value.getBytes();
    }

    public Campo(byte cod, short value) {
        this.cod = cod;
        this.value = ByteBuffer.allocate(2).putShort(value).array();
    }

    public Campo(byte cod, byte value) {
        this.cod = cod;
        this.value = ByteBuffer.allocate(1).put(value).array();
    }

    public Campo(byte cod, byte[] value) {
        this.cod = cod;
        this.value = value;

        System.out.println("..." + (byte) this.value.length);
    }

    public Campo(byte[] raw) {
        ByteBuffer rawBuffer = ByteBuffer.wrap(raw);

        this.cod = rawBuffer.get();

        byte length = rawBuffer.get();

        byte[] rawValue = new byte[length];

        rawBuffer.get(rawValue, 0, length);

        this.value = rawValue;
    }

    public byte getCod() {
        return this.cod;
    }

    public ByteBuffer getValue() {
        return ByteBuffer.wrap(this.value);
    }

    public int size() {
        return (1 + 1 + this.value.length);
    }

    public byte[] toBytes() {
        System.out.println(this.size());
        ByteBuffer ret = ByteBuffer.allocate(this.size());

        ret.put(this.cod);
        ret.put((byte) this.value.length);
        ret.put(this.value);

        return ret.array();
    }

    @Override
    public String toString() {
        ByteBuffer parsedValue = ByteBuffer.wrap(this.value);
        String printValue;

        switch (this.cod) {
            case COD_SCORE:
                printValue = new Short(parsedValue.getShort()).toString();
                break;
            default:
                printValue = new String(parsedValue.array());
        }
        return "(cod: " + this.cod + ", value: " + printValue + ")";
    }
}
