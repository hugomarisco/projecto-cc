package util;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Utilizador {
    private SimpleStringProperty alcunha;
    private SimpleIntegerProperty score;

    public Utilizador(String alcunha) {
        this.alcunha = new SimpleStringProperty(alcunha);
        this.score = new SimpleIntegerProperty(0);
    }

    public Utilizador(String alcunha, short score) {
        this.alcunha = new SimpleStringProperty(alcunha);
        this.score = new SimpleIntegerProperty(score);
    }

    public String getAlcunha() {
        return alcunha.getValue();
    }

    public short getScore() {
        return score.getValue().shortValue();
    }

    public void incrementScore(int by) {
        this.score.setValue(this.getScore() + by);
    }

    public SimpleStringProperty alcunhaProperty() {
        return alcunha;
    }

    public SimpleIntegerProperty scoreProperty() {
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Utilizador that = (Utilizador) o;

        if (!alcunha.equals(that.alcunha)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return alcunha.hashCode();
    }
}
