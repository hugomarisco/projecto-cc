package util;

import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.Image;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashSet;

public class Pergunta {
    private SimpleStringProperty pergunta;
    private ArrayList<SimpleStringProperty> opcoes;
    private int correcta;
    private Image img;

    public Pergunta(String pergunta, String o1, String o2, String o3, Integer correcta) {
        this.opcoes = new ArrayList<>();

        this.pergunta = new SimpleStringProperty(pergunta);
        this.opcoes.add(0, new SimpleStringProperty(o1));
        this.opcoes.add(1,new SimpleStringProperty(o2));
        this.opcoes.add(2,new SimpleStringProperty(o3));

        this.correcta = correcta;
    }

    public void setImgBytes(ByteBuffer imgbytes) {
        byte[] b = new byte[imgbytes.remaining()];
        imgbytes.get(b);

        this.img = new Image(new ByteArrayInputStream(b));
    }

    public Image getImg() {
        return this.img;
    }

    public String getPergunta() {
        return pergunta.get();
    }

    public SimpleStringProperty perguntaProperty() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta.set(pergunta);
    }

    public ArrayList<SimpleStringProperty> getOpcoes() {
        return this.opcoes;
    }

    public SimpleStringProperty opcao1Property() { return this.opcoes.get(0); }
    public SimpleStringProperty opcao2Property() { return this.opcoes.get(1); }
    public SimpleStringProperty opcao3Property() { return this.opcoes.get(2); }

    public int getCorrecta() {
        return correcta;
    }

    public void setCorrecta(Integer correcta) {
        this.correcta = correcta;
    }
}
