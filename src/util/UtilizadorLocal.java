package util;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

import java.net.DatagramSocket;
import java.net.InetAddress;

public class UtilizadorLocal extends Utilizador {
    private SimpleStringProperty nome, sec_info;
    private SimpleBooleanProperty bloqueado;
    private InetAddress ip;
    private int port;

    public UtilizadorLocal(String nome, String alcunha, String sec_info) {
        super(alcunha);
        this.nome = new SimpleStringProperty(nome);
        this.sec_info = new SimpleStringProperty(sec_info);
        this.bloqueado = new SimpleBooleanProperty(false);
    }

    public UtilizadorLocal(String nome, String alcunha, String sec_info, short score) {
        super(alcunha, score);
        this.nome = new SimpleStringProperty(nome);
        this.sec_info = new SimpleStringProperty(sec_info);
        this.bloqueado = new SimpleBooleanProperty(false);
    }

    public InetAddress getIp() {
        return ip;
    }

    public void setIp(InetAddress ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getNome() {
        return nome.get();
    }

    public SimpleStringProperty nomeProperty() {
        return nome;
    }

    public boolean getBloqueado() {
        return bloqueado.get();
    }

    public SimpleBooleanProperty bloqueadoProperty() {
        return bloqueado;
    }

    public void setBloqueado(boolean bloqueado) {
        this.bloqueado.set(bloqueado);
    }

    public boolean verifySecInfo(String sec_info) {
        return this.sec_info.getValue().equals(sec_info);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        UtilizadorLocal that = (UtilizadorLocal) o;

        if (!nome.equals(that.nome)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + nome.hashCode();
        return result;
    }
}
