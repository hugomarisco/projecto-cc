package server;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class MainServer {
    public static void main(String [] args) throws IOException {

        GameServer server = new GameServer(4444);

        server.start();
    }
}
