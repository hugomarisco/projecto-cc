package server;

import util.Campo;
import util.Jogo;
import util.PDU;
import util.UtilizadorLocal;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class GameWatcher implements Runnable {
    private HashMap<String, Jogo> jogos;
    private DatagramSocket socket;

    public GameWatcher(HashMap<String, Jogo> jogos, DatagramSocket socket) {
        this.jogos = jogos;
        this.socket = socket;
    }

    @Override
    public void run() {
        for (Jogo j : this.jogos.values()) {
            long data_jogo = j.getDataHora().getTime()/1000;
            long data_agora = new Date().getTime()/1000;

            if (data_jogo == data_agora) {
                System.out.println("jogo a começar");
                new Thread(new GameProcess(j, this.socket)).start();
            }
        }
    }
}
