package server;

import util.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class PDUHandler extends Thread {
    private DatagramSocket socket;
    private DatagramPacket packet;
    private HashMap<String, UtilizadorLocal> utilizadores;
    private HashMap<String, Jogo> jogos;

    public PDUHandler(DatagramSocket socket, DatagramPacket packet, HashMap<String, UtilizadorLocal> utilizadores, HashMap<String, Jogo> jogos) {
        super();

        this.socket = socket;
        this.packet = packet;
        this.utilizadores = utilizadores;
        this.jogos = jogos;
    }

    @Override
    public void run() {
        PDU receivedPDU = new PDU(this.packet.getData());

        PDU outgoingPDU = new PDU(false, PDU.TIPO_ANSWER);

        System.out.println(receivedPDU.getType());

        switch (receivedPDU.getType()) {
            case PDU.TIPO_HELLO:
                outgoingPDU.getCampos().put(Campo.COD_OK, new Campo(Campo.COD_OK, (byte) 0));

                dispatchPDU(receivedPDU, outgoingPDU);
                break;

            case PDU.TIPO_REGISTER:
                String nome_register = new String(receivedPDU.getCampos().get(Campo.COD_NOME_UTILIZADOR).getValue().array());
                String alcunha_register = new String(receivedPDU.getCampos().get(Campo.COD_ALCUNHA).getValue().array());
                String sec_info_register = new String(receivedPDU.getCampos().get(Campo.COD_SEC_INFO).getValue().array());

                if (!this.utilizadores.containsKey(alcunha_register)) {
                    UtilizadorLocal novo_utilizador = new UtilizadorLocal(nome_register, alcunha_register, sec_info_register);

                    novo_utilizador.setIp(this.packet.getAddress());
                    novo_utilizador.setPort(this.packet.getPort());

                    this.utilizadores.put(alcunha_register, novo_utilizador);

                    outgoingPDU.getCampos().put(Campo.COD_NOME_UTILIZADOR, new Campo(Campo.COD_NOME_UTILIZADOR, nome_register));
                    outgoingPDU.getCampos().put(Campo.COD_SCORE, new Campo(Campo.COD_SCORE, (short) 0));

                } else {
                    outgoingPDU.getCampos().put(Campo.COD_ERRO, new Campo(Campo.COD_ERRO, "Utilizador já registado!"));
                }

                dispatchPDU(receivedPDU, outgoingPDU);

                break;
            case PDU.TIPO_LOGIN:
                String alcunha_login = new String(receivedPDU.getCampos().get(Campo.COD_ALCUNHA).getValue().array());
                String sec_info_login = new String(receivedPDU.getCampos().get(Campo.COD_SEC_INFO).getValue().array());

                if (!this.utilizadores.containsKey(alcunha_login)) {
                    outgoingPDU.getCampos().put(Campo.COD_ERRO, new Campo(Campo.COD_ERRO, "Utilizador inexistente!"));
                } else {
                    UtilizadorLocal utilizador = this.utilizadores.get(alcunha_login);

                    utilizador.setIp(this.packet.getAddress());
                    utilizador.setPort(this.packet.getPort());

                    if (utilizador.verifySecInfo(sec_info_login)) {
                        outgoingPDU.getCampos().put(Campo.COD_NOME_UTILIZADOR, new Campo(Campo.COD_NOME_UTILIZADOR, utilizador.getAlcunha()));
                        outgoingPDU.getCampos().put(Campo.COD_SCORE, new Campo(Campo.COD_SCORE, utilizador.getScore()));
                    } else {
                        outgoingPDU.getCampos().put(Campo.COD_ERRO, new Campo(Campo.COD_ERRO, "Senha inválida!"));
                    }
                }

                dispatchPDU(receivedPDU, outgoingPDU);

                break;
            case PDU.TIPO_MAKE_CHALLENGE:
                String nome = new String(receivedPDU.getCampos().get(Campo.COD_NOME_DESAFIO).getValue().array());
                String data, hora;

                System.out.println(receivedPDU.getNumberOfFields());

                if (receivedPDU.getNumberOfFields() == 3) {
                    data = new String(receivedPDU.getCampos().get(Campo.COD_DATA).getValue().array());
                    hora = new String(receivedPDU.getCampos().get(Campo.COD_HORA).getValue().array());
                } else {
                    Date in_five_minutes = new Date(System.currentTimeMillis()+5*60*1000);

                    data = Campo.dateFormat().format(in_five_minutes);
                    hora = Campo.hourFormat().format(in_five_minutes);
                }

                try {
                    Jogo jogo = new Jogo(nome, data, hora);

                    UtilizadorLocal primeiro_utilizador = null;

                    for (UtilizadorLocal u : this.utilizadores.values()) {
                        if (u.getIp().equals(this.packet.getAddress()) && u.getPort() == this.packet.getPort())
                            primeiro_utilizador = u;
                    }

                    if (primeiro_utilizador == null) throw new Exception("O utilizador é inválido!");

                    if (this.jogos.containsKey(nome)) throw new Exception("O jogo já existe!");

                    jogo.adicionarParticipante(primeiro_utilizador);

                    this.jogos.put(nome, jogo);

                    outgoingPDU.getCampos().put(Campo.COD_NOME_DESAFIO, new Campo(Campo.COD_NOME_DESAFIO, nome));
                    outgoingPDU.getCampos().put(Campo.COD_DATA, new Campo(Campo.COD_DATA, data));
                    outgoingPDU.getCampos().put(Campo.COD_HORA, new Campo(Campo.COD_HORA, hora));
                } catch (Exception e) {
                    outgoingPDU.getCampos().put(Campo.COD_ERRO, new Campo(Campo.COD_ERRO, e.toString()));

                    e.printStackTrace();
                } finally {
                    dispatchPDU(receivedPDU, outgoingPDU);
                }

                break;
            case PDU.TIPO_LIST_CHALLENGES:
                System.out.println("received list");

                outgoingPDU = new PDU(false, PDU.TIPO_ANSWER);

                if (jogos.isEmpty()) {
                    outgoingPDU.getCampos().put(Campo.COD_ERRO, new Campo(Campo.COD_ERRO, "Não existem jogos para mostrar!"));

                    dispatchPDU(receivedPDU, outgoingPDU);
                } else {

                    Iterator<Jogo> jogos_it = this.jogos.values().iterator();

                    while (jogos_it.hasNext()) {
                        Jogo j = jogos_it.next();

                        outgoingPDU.getCampos().put(Campo.COD_NOME_DESAFIO, new Campo(Campo.COD_NOME_DESAFIO, j.getNome()));
                        outgoingPDU.getCampos().put(Campo.COD_DATA, new Campo(Campo.COD_DATA, j.getData()));
                        outgoingPDU.getCampos().put(Campo.COD_HORA, new Campo(Campo.COD_HORA, j.getHora()));
                        if (jogos_it.hasNext())
                            outgoingPDU.getCampos().put(Campo.COD_LISTA_CONTINUA, new Campo(Campo.COD_LISTA_CONTINUA, (byte) 1));

                        dispatchPDU(receivedPDU, outgoingPDU);
                    }
                }

                break;
            case PDU.TIPO_LIST_RANKING:
                Iterator<UtilizadorLocal> utilizadores_it = this.utilizadores.values().iterator();

                while (utilizadores_it.hasNext()) {
                    Utilizador u = utilizadores_it.next();

                    outgoingPDU = new PDU(false, PDU.TIPO_ANSWER);

                    outgoingPDU.getCampos().put(Campo.COD_ALCUNHA, new Campo(Campo.COD_ALCUNHA, u.getAlcunha()));
                    outgoingPDU.getCampos().put(Campo.COD_SCORE, new Campo(Campo.COD_SCORE, u.getScore()));
                    if (utilizadores_it.hasNext()) outgoingPDU.getCampos().put(Campo.COD_LISTA_CONTINUA, new Campo(Campo.COD_LISTA_CONTINUA, (byte) 1));

                    dispatchPDU(receivedPDU, outgoingPDU);
                }

                break;
            case PDU.TIPO_DELETE_CHALLENGE:
                String nome_desafio = new String(receivedPDU.getCampos().get(Campo.COD_NOME_DESAFIO).getValue().array());

                this.jogos.remove(nome_desafio);

                outgoingPDU.getCampos().put(Campo.COD_ERRO, new Campo(Campo.COD_ERRO, "Jogo cancelado!"));

                dispatchPDU(receivedPDU, outgoingPDU);

                break;
            case PDU.TIPO_ACCEPT_CHALLENGE:

                break;
        }
    }

    private void dispatchPDU(PDU receivedPDU, PDU outgoingPDU) {
        outgoingPDU.setLabel(receivedPDU.getLabel());

        DatagramPacket packet = new DatagramPacket(outgoingPDU.toBytes(), outgoingPDU.length(), this.packet.getAddress(), this.packet.getPort());

        System.out.println("received packet from " + this.packet.getAddress() + " " + this.packet.getPort());

        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
