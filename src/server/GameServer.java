package server;

import util.Jogo;
import util.PDU;
import util.UtilizadorLocal;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class GameServer extends Thread {
    private DatagramSocket socket;
    private HashMap<String, UtilizadorLocal> utilizadores;
    private HashMap<String, Jogo> jogos;

    public GameServer(int port) throws SocketException {
        this.socket = new DatagramSocket(port);
        this.utilizadores = new HashMap<>();
        this.jogos = new HashMap<>();

        System.out.println("Server is ready on port " + port);
    }

    @Override
    public void run() {
        System.out.println("Server is running...");

        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(new GameWatcher(this.jogos, this.socket), 0, 1, TimeUnit.SECONDS);

        while (true) {
            DatagramPacket incomingPacket = new DatagramPacket(new byte[PDU.PACKETSIZE], PDU.PACKETSIZE);

            try {
                this.socket.receive(incomingPacket);

                new PDUHandler(this.socket, incomingPacket, this.utilizadores, this.jogos).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
