package server;

import javafx.beans.property.SimpleStringProperty;
import util.*;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class GameProcess implements Runnable {

    private Jogo jogo;
    private DatagramSocket socket;

    public GameProcess(Jogo jogo, DatagramSocket socket) {
        this.jogo = jogo;
        this.socket = socket;
    }

    @Override
    public void run() {
        ArrayList<PDU> pduList = new ArrayList<>();

        PDU outgoingPDU = new PDU(false, PDU.TIPO_ANSWER);

        outgoingPDU.setLabel((short) 0);

        Pergunta p = jogo.proximaPergunta();

        outgoingPDU.getCampos().put(Campo.COD_NOME_DESAFIO, new Campo(Campo.COD_NOME_DESAFIO, this.jogo.getNome()));
        outgoingPDU.getCampos().put(Campo.COD_NR_QUESTAO, new Campo(Campo.COD_NR_QUESTAO, (short) this.jogo.getNrPergunta()));
        outgoingPDU.getCampos().put(Campo.COD_QUESTAO, new Campo(Campo.COD_QUESTAO, p.getPergunta()));
        outgoingPDU.getCampos().put(Campo.COD_RESPOSTA1, new Campo(Campo.COD_RESPOSTA1, p.getOpcoes().get(0).get()));
        outgoingPDU.getCampos().put(Campo.COD_RESPOSTA2, new Campo(Campo.COD_RESPOSTA2, p.getOpcoes().get(1).get()));
        outgoingPDU.getCampos().put(Campo.COD_RESPOSTA3, new Campo(Campo.COD_RESPOSTA3, p.getOpcoes().get(2).get()));
        outgoingPDU.getCampos().put(Campo.COD_CERTA, new Campo(Campo.COD_CERTA, (short) p.getCorrecta()));
        outgoingPDU.getCampos().put(Campo.COD_LISTA_CONTINUA, new Campo(Campo.COD_LISTA_CONTINUA, (byte) 1));

        dispatchPDU(outgoingPDU);

        try {
            byte[] imgbytes = Files.readAllBytes(Paths.get(this.jogo.imagemActualPath()));

            int start = 0;

            for (int i=0; i<imgbytes.length; i+=Math.min(101,imgbytes.length-i)) {
                outgoingPDU = new PDU(false, PDU.TIPO_ANSWER);

                outgoingPDU.getCampos().put(Campo.COD_IMAGEM, new Campo(Campo.COD_IMAGEM, Arrays.copyOfRange(imgbytes, i, i+Math.min(100,imgbytes.length-i-1))));

                if (i+Math.min(101,imgbytes.length-i) != imgbytes.length)
                    outgoingPDU.getCampos().put(Campo.COD_LISTA_CONTINUA, new Campo(Campo.COD_LISTA_CONTINUA, (byte) 1));

                dispatchPDU(outgoingPDU);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("pdu pronto");

        Executors.newScheduledThreadPool(1).schedule(new GameProcess(this.jogo, this.socket), 1, TimeUnit.MINUTES);
    }

    public void dispatchPDU(PDU outgoingPDU) {
        for (UtilizadorLocal u : this.jogo.getParticipantes()) {
            DatagramPacket packet = new DatagramPacket(outgoingPDU.toBytes(), outgoingPDU.length(), u.getIp(), u.getPort());

            try {
                this.socket.send(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
