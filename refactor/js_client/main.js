var BrowserWindow, app, dgram, mainWindow;

app = require('app');

BrowserWindow = require('browser-window');

dgram = require('dgram');

mainWindow = null;

app.on('window-all-closed', function() {
  if (process.platform !== 'darwin') {
    return app.quit();
  }
});

app.on('ready', function() {
  mainWindow = new BrowserWindow({
    width: 1024,
    height: 1024
  });
  mainWindow.loadUrl('file://' + __dirname + '/index.html');
  mainWindow.openDevTools();
  return mainWindow.on('closed', function() {
    return mainWindow = null;
  });
});
