gulp = require('gulp');
coffee = require('gulp-coffee');
gutil = require('gulp-util');

gulp.task('coffee', function() {
  gulp.src('./coffee/*.coffee')
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(gulp.dest('.'))
});

gulp.task('watch', function() {
	gulp.watch('./coffee/*.coffee', ['coffee']);
});

gulp.task('default', ['watch']);