app = require 'app'
BrowserWindow = require 'browser-window'
dgram = require 'dgram'

mainWindow = null

app.on 'window-all-closed', ->
  app.quit() if process.platform != 'darwin'

app.on 'ready', ->
  mainWindow = new BrowserWindow({width: 1024, height: 1024})

  mainWindow.loadUrl('file://' + __dirname + '/index.html')

  mainWindow.openDevTools()

  mainWindow.on 'closed', ->
    mainWindow = null