require "resolv"
require "socket"

abort("./server TCP_PORT UDP_PORT [SERVER_IP:SERVER_PORT]") if ARGV.length < 2

tcp_port = ARGV[0].to_i
udp_port = ARGV[1].to_i

abort("Invalid TCP/UDP port(s)") if tcp_port == 0 || udp_port == 0

if ARGV.length >= 3
	upstream_server = ARGV[2].split(':')
	upstream_server_ip = upstream_server[0]
	upstream_server_port = upstream_server[1].to_i

	abort("Invalid IP/PORT for upstream server") if !(upstream_server_ip =~ Resolv::IPv4::Regex) || upstream_server_port == 0
end

puts "Started UDP server on #{udp_port}..."
 
Socket.udp_server_loop(udp_port) { |msg, msg_src|
	puts msg
}